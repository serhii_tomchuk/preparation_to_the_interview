## Idempotence
- [x] [Idempotence1](https://en.wikipedia.org/wiki/Idempotence)
- [x] [Idempotence2](https://www.youtube.com/watch?v=ZzQfxoMFH0U)
- [x] [Idempotence3](https://www.youtube.com/watch?v=5qQQ3yzbKp8)

## Authentication
Authentication is the process of verifying the identity of a user or process. Authentication is commonly performed by submitting a user name or ID and one or more items of private information that only a given user should know.

## Authorization
Authorization is the process of giving someone permission to do or have something. In multi-user computer systems, a system administrator defines for the system which users are allowed access to the system and what privileges of use (such as access to which file directories, hours of access, amount of allocated storage space, and so forth).

## Identification
Identification is the process of associating or identifying a user, machine, or other entity as having a unique identity. The identifier is a unique symbol. The noun identifier refers to the symbol itself, whereas the verb identify means to assign a symbol to the object in question.

## Encryption
Encryption is the process of encoding a message or information in such a way that only authorized parties can access it and those who are not authorized cannot. Encryption does not itself prevent interference but denies the intelligible content to a would-be interceptor.

## Hashing
Hashing is the transformation of a string of characters into a usually shorter fixed-length value or key that represents the original string. Hashing is used to index and retrieve items in a database because it is faster to find the item using the shorter hashed key than to find it using the original value.

## Symmetric Encryption
Symmetric encryption is a type of encryption where only one key (a secret key) is used to both encrypt and decrypt electronic information. The entities communicating via symmetric encryption must exchange the key so that it can be used in the decryption process.

## Asymmetric Encryption
Asymmetric encryption is a type of encryption that uses two separate keys to encrypt and decrypt data. The two keys are related mathematically, but are not identical. One key in the pair can be shared with everyone; it is called the public key. The other key in the pair is kept secret; it is called the private key.
