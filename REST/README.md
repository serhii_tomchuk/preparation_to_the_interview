# REST: Representational State Transfer

------------------------
1. Model client-server interactions 
2. Simplicity 
3. Stateless 
4. Cacheable 
5. Uniform interface 
6. Layered system 
7. Code on demand (optional)
------------------------

## What is REST?
REST is an architectural style for building distributed systems based on 
hypermedia. REST is independent of any underlying protocol and is not 
necessarily tied to HTTP.

## What is a RESTful API?
A RESTful API is an application program interface (API) that uses HTTP requests 
to GET, PUT, POST and DELETE data.

## RESTful API Design
### Resources
A resource is an object with a type, associated data, relationships to other 
resources, and a set of methods that operate on it. Resources are exposed as
nouns.

### Methods
A method is a type of request that can be made to a resource. Methods are
exposed as verbs.

### Representations
A representation is a format for the resource data. Representations are exposed
as content types.

### URIs
A URI is a unique identifier for a resource. URIs are exposed as endpoints.

### HTTP Methods
| Method | Description | Idempotent | Safe | Cacheable | Idempotent + Safe | Idempotent + Safe + Cacheable |
| ------ | ----------- | ---------- | ---- | --------- | ----------------- | ------------------------------ |
| GET    | Retrieve a resource or collection of resources | Yes | Yes | Yes | Yes | Yes |
| POST   | Create a new resource | No | No | Yes | No | No |
| PUT    | Update an existing resource | Yes | No | No | No | No |
| PATCH  | Update an existing resource | No | No | No | No | No |
| DELETE | Delete a resource | Yes | No | No | No | No |
| HEAD   | Retrieve the headers for a resource | Yes | Yes | Yes | Yes | Yes |
| OPTIONS | Retrieve the methods that can be performed on a resource | Yes | Yes | Yes | Yes | Yes |

### HTTP Status Codes
| Code | Description | Meaning | Example | 
| ---- | ----------- | ------- | ------- |
| 1xx | Informational | Request received, continuing process | 100 Continue |
| 2xx | Success | The action was successfully received, understood, and accepted | 200 OK |
| 3xx | Redirection | Further action must be taken in order to complete the request | 301 Moved Permanently |
| 4xx | Client Error | The request contains bad syntax or cannot be fulfilled | 400 Bad Request |
| 5xx | Server Error | The server failed to fulfill an apparently valid request | 500 Internal Server Error |

### HTTP Headers
| Header | Description | Example | 
| ------ | ----------- | ------- |
| Accept | The content types that are acceptable for the response | Accept: application/json |
| Accept-Charset | The character sets that are acceptable for the response | Accept-Charset: utf-8 |
| Accept-Encoding | The content encodings that are acceptable for the response | Accept-Encoding: gzip, deflate |
| Accept-Language | The natural languages that are acceptable for the response | Accept-Language: en-US |
