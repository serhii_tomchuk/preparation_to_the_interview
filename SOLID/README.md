# SOLID

1. [Single Responsibility Principle](#single-responsibility-principle)
2. [Open/Closed Principle](#openclosed-principle)
3. [Liskov Substitution Principle](#liskov-substitution-principle)
4. [Interface Segregation Principle](#interface-segregation-principle)
5. [Dependency Inversion Principle](#dependency-inversion-principle)

## Single Responsibility Principle

> A class should have only one reason to change.
> 
> Robert C. Martin
> 
> [Wikipedia](https://en.wikipedia.org/wiki/Single_responsibility_principle)


## Open/Closed Principle
> Software entities (classes, modules, functions, etc.) should be open for extension, but closed for modification.
> 
> Robert C. Martin
> 
> [Wikipedia](https://en.wikipedia.org/wiki/Open%E2%80%93closed_principle)

## Liskov Substitution Principle
> Objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program.
> 
> Robert C. Martin
> 
> [Wikipedia](https://en.wikipedia.org/wiki/Liskov_substitution_principle)

## Interface Segregation Principle
> Many client-specific interfaces are better than one general-purpose interface.
> 
> Robert C. Martin
> 
> [Wikipedia](https://en.wikipedia.org/wiki/Interface_segregation_principle)

## Dependency Inversion Principle
> One should depend upon abstractions, [not] concretions.
> 
> Robert C. Martin

> Abstractions should not depend upon details. Details should depend upon abstractions.
> 
> Robert C. Martin
