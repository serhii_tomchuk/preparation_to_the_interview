"""
6 kyu   --> Sum of Digits / Digital Root
16      -->  1 + 6 = 7
942     -->  9 + 4 + 2 = 15  -->  1 + 5 = 6
132189  -->  1 + 3 + 2 + 1 + 8 + 9 = 24  -->  2 + 4 = 6
493193  -->  4 + 9 + 3 + 1 + 9 + 3 = 29  -->  2 + 9 = 11  -->  1 + 1 = 2
"""


def digital_root(n: int) -> int:
    while len(str(n)) > 1:
        return digital_root(sum([int(i) for i in str(n)]))
    return n


if __name__ == '__main__':
    print(digital_root(16))
    print(digital_root(942))
    print(digital_root(132189))
    print(digital_root(493193))