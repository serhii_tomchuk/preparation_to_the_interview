a = frozenset([1, 2, 3])
b = frozenset([3, 4, 5])

# copy() returns a shallow copy of the set
print(a.copy())
# difference() returns a set containing the difference between two or more sets
print(a.difference(b))
print(b.difference(a))
# intersection() returns a set, that is the intersection of two other sets
print(a.intersection(b))
# isdisjoint() returns whether two sets have a intersection or not
print(a.isdisjoint(b))
# issubset() returns whether another set contains this set or not
print(a.issubset(b))
# issuperset() returns whether this set contains another set or not
print(a.issuperset(b))
# symmetric_difference() returns a set with the symmetric differences of two
# sets
print(a.symmetric_difference(b))
# union() returns a set containing the union of sets
print(a.union(b))
