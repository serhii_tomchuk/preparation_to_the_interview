str_: str = "Hello, World!"
# slices
print(str_[0:5])  # Hello
print(str_[7:12])  # World
print(str_[0:12])  # Hello, World
print(str_[0:12:2])  # Hlo ol
print(str_[0:12:3])  # Hl r
print(str_[0:12:4])  # Hoo
print(str_[0:12:5])  # H,Wl
print(str_[0:12:6])  # Hr
print(str_[0:12:7])  # Hl
print(str_[0:12:8])  # H
print(str_[0:12:9])  # H

# negative slices
print(str_[-1])  # !
print(str_[-2])  # d
print(str_[-3])  # l
print(str_[0: -4])  # r

# reverse string
print(str_[::-1])  # !dlroW ,olleH

# string concatenation
print("Hello" + "World")  # HelloWorld
print("Hello" + " " + "World")  # Hello World
print("Hello" + ", " + "World!")  # Hello, World!
print("Hello" + ", " + "World" + "!")  # Hello, World!

# string multiplication
print("Hello" * 5)  # HelloHelloHelloHelloHello
print("Hello" * 5 + "World")  # HelloHelloHelloHelloHelloWorld

# string formatting
print(f"{str_}")  # Hello, World!
print("{}".format(str_))  # Hello, World!
print("{0}".format(str_))  # Hello, World!

# string formatting with placeholders
print("%s" % str_)  # Hello, World!
print("%s %s" % ("Hello", "World"))  # Hello World

# string's length
print(len(str_))  # 13

# string's methods

print(str_.upper())  # HELLO, WORLD!
print(str_.capitalize())  # Hello, world!
print(str_.casefold())  # hello, world!
print(str_.center(20))  #   Hello, World!
print(str_.count("l"))  # 3
print(str_.encode())  # b'Hello, World!'
print(str_.endswith("!"))  # True
print(str_.endswith("d!"))  # True
print(str_.split(","))  # ['Hello', ' World!']
print(str_.strip("!"))  # Hello, World
print(str_.find("o"))  # 4
print(str_.index("o"))  # 4
print(str_.isalnum())  # False
print(str_.isalpha())  # False
print(str_.isascii())  # True
print(str_.isdecimal())  # False
print(str_.isdigit())  # False
print(str_.isidentifier())  # False
print(str_.islower())  # False
print(str_.isnumeric())  # False
print(str_.isprintable())  # True
print(str_.isspace())  # False
print(str_.istitle())  # True
