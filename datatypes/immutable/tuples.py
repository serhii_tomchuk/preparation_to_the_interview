tpl = (
    1, "hello", 3.4,
    True,
    {i: i * 2 for i in range(10)},
    (1, 2, 3),
    [1, 2, 3]
)

# indexes
print(tpl[0])
print(tpl[-1])

# slicing
print(tpl[1:3])
print(tpl[:3])
print(tpl[3:])
print(tpl[::2])

# concatenation
print(tpl + tpl)
print(tpl * 3)

# membership
print(1 in tpl)
print(1 not in tpl)
print(10 in tpl)

# length
print(len(tpl))

# methods
print(tpl.count(1))
print(tpl.index(1))

# unpacking
a, b, c, d, e, f, g = tpl
print(a)
print(b)
print(c)
print(d)
print(e)
print(f)
print(g)

# iteration
for i in tpl:
    print(i)

# iteration with index
for i, v in enumerate(tpl):
    print(i, v)

# iteration with index and start
for i, v in enumerate(tpl, 1):
    print(i, v)

# comparison
print((1, 2, 3) == (1, 2, 3))

# comparison with different types
print((1, 2, 3) == [1, 2, 3])

tpl_gen = (i for i in range(5))
print(tpl_gen, type(tpl_gen))
print(tuple(tpl_gen))

# iteration from generator
for i in tpl_gen:
    print(i)
