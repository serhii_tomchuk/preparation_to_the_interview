# Dict
dct = {'key1': 'value1', 'key2': 'value2'}

# data manipulation
dct['key1'] = 'new value'
dct['key3'] = 'value3'
del dct['key2']

# data access
print(dct['key1'])
print(dct.get('key1'))
print(dct.get('key2'))
print(dct.get('key2', 'default value'))

# data iteration
for key in dct:
    print(key, dct[key])

for key, value in dct.items():
    print(key, value)

# data membership
print('key1' in dct)

# data length
print(len(dct))

# data type
print(type(dct))

# data copy
dct2 = dct.copy()

# data clear
dct.clear()

# data update
dct.update(dct2)

# data keys
print(dct.keys())

# data values
print(dct.values())

# data pop
print(dct.pop('key1'))

# data popitem
print(dct.popitem())

# data setdefault
print(dct.setdefault('key1', 'value1'))

# data fromkeys
print(dict.fromkeys(['key1', 'key2']))
