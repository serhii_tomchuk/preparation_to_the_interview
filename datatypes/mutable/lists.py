# List

lst: list = [1, 2, 3, 4, 5]

# Accessing elements
print(lst[0])  # 1

# Slicing
print(lst[1:3])  # [2, 3]
print(lst[1:])  # [2, 3, 4, 5]
print(lst[:3])  # [1, 2, 3]
print(lst[:])  # [1, 2, 3, 4, 5]
print(lst[::2])  # [1, 3, 5]
print(lst[::-1])  # [5, 4, 3, 2, 1]

# Adding elements
lst.append(6)
print(lst)  # [1, 2, 3, 4, 5, 6]

lst.insert(0, 0)
print(lst)  # [0, 1, 2, 3, 4, 5, 6]

lst.extend([7, 8, 9])
print(lst)  # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

# Removing elements
lst.pop()
print(lst)  # [0, 1, 2, 3, 4, 5, 6, 7, 8]

# Removing elements by index
lst.pop(0)
print(lst)  # [1, 2, 3, 4, 5, 6, 7, 8]

# Removing elements by value
lst.remove(2)
print(lst)  # [1, 3, 4, 5, 6, 7, 8]

# Removing all elements
lst.clear()
print(lst)  # []

# Reversing
lst = [1, 2, 3, 4, 5]
lst.reverse()
print(lst)  # [5, 4, 3, 2, 1]

# Sorting
lst = [5, 3, 1, 4, 2]
lst.sort()
print(lst)  # [1, 2, 3, 4, 5]

# Sorting in reverse
lst.sort(reverse=True)
print(lst)  # [5, 4, 3, 2, 1]

# Sorting with key
lst = ["abc", "ab", "abcd"]
lst.sort(key=len)
print(lst)  # ['ab', 'abc', 'abcd']

# Sorting with key in reverse
lst.sort(key=len, reverse=True)
print(lst)  # ['abcd', 'abc', 'ab']

# Counting elements
lst = [1, 2, 3, 4, 5, 1, 2, 3, 1]
print(lst.count(1))  # 3

# Finding index of element
print(lst.index(3))  # 2

# Finding index of element with start and end
print(lst.index(3, 3, 8))  # 7

# Copying
lst2 = lst.copy()
print(lst2)  # [1, 2, 3, 4, 5, 1, 2, 3, 1]

# List comprehension
lst = [i for i in range(10)]

# List comprehension with if
lst = [i for i in range(10) if i % 2 == 0]

# List comprehension with if else
lst = [i if i % 2 == 0 else -1 for i in range(10)]

# List comprehension with nested for
lst = [i + j for i in range(10) for j in range(10)]

# List comprehension with nested for and if
lst = [i + j for i in range(10) for j in range(10) if i + j < 10]

# List comprehension with nested for and if else
lst = [i + j if i + j < 10 else -1 for i in range(10) for j in range(10)]
