# sets are mutable
a = {i for i in range(10)}

# data manipulation
a.add(10)
print(a)
a.remove(0)
print(a)
a.discard(1)
print(a)
a.pop()
print(a)
a.clear()
print(a)

# set operations
a = {1, 2, 3, 4, 5}
b = {4, 5, 6, 7, 8}

# union
print(a | b)
print(a.union(b))

# intersection
print(a & b)
print(a.intersection(b))

# difference
print(a - b)
print(a.difference(b))

# symmetric difference
print(a ^ b)
print(a.symmetric_difference(b))

# subset
print(a <= b)
print(a.issubset(b))

# superset
print(a >= b)
print(a.issuperset(b))

# disjoint
print(a.isdisjoint(b))
