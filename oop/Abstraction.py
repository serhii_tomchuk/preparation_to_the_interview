"""
OOP: Abstraction.
-----------------
Abstraction is the process of hiding the real implementation of an application
from the user and emphasizing only on usage of it. In Python, abstraction can
be achieved by using abstract classes and interfaces. An abstract class is a
class that generally provides incomplete functionality and contains one or
more abstract methods. An abstract method is a method that is declared, but
contains no implementation. Abstract classes may not be instantiated, and its
abstract methods must be implemented by its subclasses.
"""
from abc import ABC, abstractmethod

import requests


class Request(ABC):
    """Abstract class for Requester."""

    def __init__(self):
        self.session = None
        super().__init__()

    @abstractmethod
    def get(self, url, params=None, **kwargs):
        pass

    @abstractmethod
    def post(self, url, params=None, **kwargs):
        pass

    @abstractmethod
    def put(self, url, params=None, **kwargs):
        pass

    @abstractmethod
    def delete(self, url, params=None, **kwargs):
        pass


class Requester(Request):
    """Requester class."""

    def __init__(self):
        super().__init__()

    def __get_session(self):
        return self.session if self.session is not None else requests.Session()

    def get(self, url, params=None, **kwargs):
        return self.__get_session().get(url, **kwargs)

    def post(self, url, params=None, **kwargs):
        return self.__get_session().post(url, **kwargs)

    def put(self, url, params=None, **kwargs):
        return self.__get_session().put(url, **kwargs)

    def delete(self, url, params=None, **kwargs):
        return self.__get_session().delete(url, **kwargs)


if __name__ == "__main__":
    requester = Requester()
    response = requester.get("https://www.google.com")
    print(response.status_code)
