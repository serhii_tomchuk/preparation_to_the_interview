"""
OOP: Encapsulation.
Encapsulation is the process of restricting access to methods and variables in
a class in order to prevent direct data modification, so it prevents accidental
modification of data.
"""


class Computer:

    def __init__(self):
        self.__maxp_rice = 900

    def sell(self):
        print("Selling Price: {}".format(self.__maxp_rice))

    def set_max_price(self, price):
        self.__maxp_rice = price


c = Computer()
c.sell()

# change the price
c.__maxp_rice = 1000 # this will not change the price because of encapsulation
c.sell()

# using setter function
c.set_max_price(1000)
c.sell()
