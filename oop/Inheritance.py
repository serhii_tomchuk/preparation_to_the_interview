"""
OOP: Inheritance.
Inheritance allows us to define a class that inherits all the methods and
properties from another class.
"""


# Create a Parent Class
# Any class can be a parent class, so the syntax is the same as creating any
# other class.
class Person:
    def __init__(self, fname, lname):
        self.firstname = fname
        self.lastname = lname

    def printname(self):
        print(self.firstname, self.lastname)


class Student(Person):
    pass


x = Student("Mike", "Olsen")
x.printname()


class Kid(Person):
    def __init__(self, fname, lname, year):
        super().__init__(fname, lname)
        self.graduationyear = year

    def welcome(self):
        print(
            "Welcome", self.firstname, self.lastname,
            "to the class of", self.graduationyear)


y = Kid("Mike", "Olsen", 2019)
y.welcome()
