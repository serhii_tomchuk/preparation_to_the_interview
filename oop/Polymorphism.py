"""
OOP: Polymorphism.
Polymorphism is the ability to leverage the same interface for different
underlying forms such as data types or classes.
"""


# Example 1: Polymorphism with a Function and Objects
class Shark:
    def swim(self):
        print("The shark is swimming.")

    def swim_backwards(self):
        print("The shark cannot swim backwards, but can sink backwards.")

    def skeleton(self):
        print("The shark's skeleton is made of cartilage.")


class Clownfish:
    def swim(self):
        print("The clownfish is swimming.")

    def swim_backwards(self):
        print("The clownfish can swim backwards.")

    def skeleton(self):
        print("The clownfish's skeleton is made of bone.")


# Common interface
class Fish:
    def __init__(self, name):
        self.name = name

    def swim(self):
        print(self.name + " is swimming.")

    def swim_backwards(self):
        print(self.name + " can swim backwards.")


# In the Pacific Ocean
def in_the_pacific(fish):
    fish.swim()
    fish.skeleton()
    fish.swim_backwards()


# Instantiate objects
shark = Shark()
clownfish = Clownfish()

# Passing the objects
in_the_pacific(shark)
in_the_pacific(clownfish)

# Example 2: Polymorphism with Abstract Classes and Inheritance
from abc import ABC, abstractmethod


class Polygon(ABC):
    @abstractmethod
    def no_of_sides(self):
        pass


class Triangle(Polygon):
    # overriding abstract method
    def no_of_sides(self):
        print("I have 3 sides.")


class Pentagon(Polygon):
    # overriding abstract method
    def no_of_sides(self):
        print("I have 5 sides.")


class Hexagon(Polygon):
    # overriding abstract method
    def no_of_sides(self):
        print("I have 6 sides.")


class Quadrilateral(Polygon):
    # overriding abstract method
    def no_of_sides(self):
        print("I have 4 sides.")
