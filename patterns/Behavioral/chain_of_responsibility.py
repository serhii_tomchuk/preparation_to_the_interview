"""
PATTERN: Chain of Responsibility.

INTENT:
    Avoid coupling the sender of a request to its receiver by giving more than
    one object a chance to handle the request.
    Chain the receiving objects and pass the request along the chain until an
    object handles it.
    Launch-and-leave requests with a single processing pipeline that contains
    many possible handlers.
    An object-oriented linked list with recursive traversal.
APPLICABILITY:
    more than one object may handle a request, and the handler isn't known a
    priori.
    you want to issue a request to one of several objects without specifying
    the receiver explicitly.
    the set of objects that can handle a request should be specified
    dynamically.
"""


class Handler:
    """
    Define an interface for handling requests.
    Implement the successor link.
    """

    def __init__(self, successor):
        self.successor = successor

    def handle(self, request):
        pass


class ConcreteHandler1(Handler):
    """
    Handle requests it is responsible for.
    Can access its successor.
    If the ConcreteHandler can handle the request, it does so; otherwise it
    forwards the request to its successor.
    """

    def handle(self, request):
        if request == "run":
            print("ConcreteHandler1 handles request: " + request)
        else:
            self.successor.handle(request)


class ConcreteHandler2(Handler):
    """
    Handle requests it is responsible for.
    Can access its successor.
    If the ConcreteHandler can handle the request, it does so; otherwise it
    forwards the request to its successor.
    """

    def handle(self, request):
        if request == "jump":
            print("ConcreteHandler2 handles request: " + request)
        else:
            self.successor.handle(request)


class ConcreteHandler3(Handler):
    """
    Handle requests it is responsible for.
    Can access its successor.
    If the ConcreteHandler can handle the request, it does so; otherwise it
    forwards the request to its successor.
    """

    def handle(self, request):
        if request == "swim":
            print("ConcreteHandler3 handles request: " + request)
        else:
            self.successor.handle(request)


class DefaultHandler(Handler):
    """
    Handle requests it is responsible for.
    Can access its successor.
    If the ConcreteHandler can handle the request, it does so; otherwise it
    forwards the request to its successor.
    """

    def handle(self, request):
        print("End of chain, no handler for {}".format(request))


class Client:
    """
    Initiates the request to a ConcreteHandler object on the chain.
    """

    def __init__(self):
        self.handler = ConcreteHandler1(
            ConcreteHandler2(
                ConcreteHandler3(
                    DefaultHandler(None)
                )
            )
        )

    def delegate(self, requests):
        for request in requests:
            self.handler.handle(request)


if __name__ == "__main__":
    client = Client()
    requests = ["run", "jump", "swim", "fall", "fly"]
    client.delegate(requests)
