"""
PATTERN: Command.

INTENT:
    Encapsulate a request as an object, thereby letting you parameterize clients
    with different requests, queue or log requests, and support undoable
    operations.
    Promote "invocation of a method on an object" to full object status.
    An object-oriented callback.
APPLICABILITY:
    parameterize objects by an action to perform.
    specify, queue, and execute requests at different times.
    support undo.
    support logging changes so that they can be reapplied in case of a system
    crash.
    structure a system around high-level operations built on primitives
    operations.
"""

import abc


class Command(metaclass=abc.ABCMeta):
    """
    Declare an interface for executing an operation.
    """

    def __init__(self, receiver):
        self.receiver = receiver

    @abc.abstractmethod
    def execute(self):
        pass

    @abc.abstractmethod
    def undo(self):
        pass

    @abc.abstractmethod
    def redo(self):
        pass


class ConcreteCommand(Command):

    def __init__(self, receiver):
        super().__init__(receiver)

    def execute(self):
        self.receiver.action()

    def undo(self):
        self.receiver.undo()

    def redo(self):
        self.execute()


class Receiver:

    def action(self):
        print("Receiver action")

    def undo(self):
        print("Receiver undo")

    def redo(self):
        print("Receiver redo")


class Invoker:

    def __init__(self):
        self._history = []

    def store_command(self, command):
        self._history.append(command)

    def execute(self):
        for command in self._history:
            command.execute()

    def undo(self):
        for command in reversed(self._history):
            command.undo()

    def redo(self):
        for command in self._history:
            command.redo()

    def clear_history(self):
        self._history = []


def main():
    receiver = Receiver()
    command = ConcreteCommand(receiver)
    invoker = Invoker()
    invoker.store_command(command)
    invoker.execute()
    invoker.undo()
    invoker.redo()
    invoker.clear_history()


if __name__ == "__main__":
    main()
