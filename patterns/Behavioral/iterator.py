"""
PATTERN: Iterator.

INTENT:
    Iterator is a behavioral design pattern that lets you traverse elements of
    a collection without exposing its underlying representation
    (list, stack, tree, etc.).

APPLICABILITY:
    * Use the Iterator pattern when your collection has a complex data
    structure under the hood, but you want to hide
      its complexity from clients (either for convenience or security reasons).
    * Use the pattern to reduce duplication of the traversal code across your
    app.
    * Use the Iterator when you want your code to be able to traverse different
    data structures or when types of these structures are unknown beforehand.
"""

from __future__ import annotations

from collections.abc import Iterable, Iterator
from typing import Any, List


class AlphabeticalOrderIterator(Iterator):
    """
    Concrete Iterators implement various traversal algorithms. These classes
    store the current traversal position at all times.
    """

    _position: int = None
    _reverse: bool = False

    def __init__(self, collection: WordsCollection,
                 reverse: bool = False) -> None:
        self._collection: WordsCollection = collection
        self._reverse = reverse
        self._position = -1 if reverse else 0

    def __next__(self) -> str:
        """
        The __next__() method must return the next item in the sequence. On
        reaching the end, and in subsequent calls, it must raise StopIteration.
        """
        try:
            value = self._collection[self._position]
            self._position += -1 if self._reverse else 1
        except IndexError:
            raise StopIteration()

        return value


class WordsCollection(Iterable):

    def __init__(self, collection: List[Any] = []) -> None:
        self._collection = collection

    def __iter__(self) -> AlphabeticalOrderIterator:
        """
        The __iter__() method returns the iterator object itself, by default
        we return the iterator in ascending order.
        """
        return AlphabeticalOrderIterator(self._collection)

    def get_reverse_iterator(self) -> AlphabeticalOrderIterator:
        return AlphabeticalOrderIterator(self._collection, True)

    def add_item(self, item: Any):
        self._collection.append(item)


def client_code():
    """
    The client code may or may not know about the Concrete Iterator or
    Collection classes, depending on the level of indirection you want to keep
    in your program.
    """
    collection = WordsCollection()
    collection.add_item("First")
    collection.add_item("Second")
    collection.add_item("Third")

    print("Straight traversal:")
    print("\n".join(collection))
    print("")

    print("Reverse traversal:")
    print("\n".join(collection.get_reverse_iterator()), end="")


if __name__ == "__main__":
    client_code()
